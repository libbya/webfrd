const COLOR_SQUARE_ERR_THRESHOLD = 4000;

let selectingBoundary = null;
let boundaries = {
    left: null,
    right: null,
    top: null,
    bottom: null
}
let curImgBlob = null;
const canvas = document.getElementById('graph-canvas');

/* Update the canvas when the user pastes an image from the clipboard */
window.addEventListener('paste', evt => {
    var {items} = evt.clipboardData;
    const img = [...items].find(el => el.kind === 'file' && el.type.startsWith('image'));
    const blob = img ? img.getAsFile() : null;
    if (img)
        document.getElementById('msg-container').innerText = "Select a boundary and click to place it on the graph";
    updateCanvas(blob);
}, false);

/* Update the canvas when the user uploads a file using the file picker */
document.getElementById('file-picker').addEventListener('change', evt => {
    document.getElementById('msg-container').innerText = "Select a boundary and click to place it on the graph";
    const blob = document.getElementById('file-picker').files[0];
    updateCanvas(blob);
}, false);

/* Enter boundary select state */
for (let dir of ['top', 'bottom', 'left', 'right']) {
    const btn = document.getElementById(`${dir}-boundary-select`);
    btn.addEventListener('click', evt => {
        btn.classList.add('selecting');
        for (let otherDir of ['top', 'bottom', 'left', 'right'].filter(el => el !== dir)) {
            document.getElementById(`${otherDir}-boundary-select`).classList.remove('selecting');
        }
        selectingBoundary = dir;
    }, false);
}

/* Reset the boundaries & redraw the canvas to remove boundary lines */
document.getElementById('boundary-reset').addEventListener('click', evt => {
    document.getElementById('msg-container').innerText = "Select a boundary and click to place it on the graph";
    boundaries = {
        left: null,
        right: null,
        top: null,
        bottom: null
    }
    selectingBoundary = null;
    for (let dir of ['top', 'bottom', 'left', 'right']) {
        document.getElementById(`${dir}-boundary-select`).classList.remove('selected', 'selecting');
        //document.getElementById(`${dir}-boundary-select`).classList.remove('selecting');
    }
    updateCanvas(curImgBlob);
    document.getElementById('download-button').setAttribute('disabled', "true")
    document.getElementById('download-link').setAttribute('href', '#');
    document.getElementById('download-link').removeAttribute('download');
}, false);

/* Replace the canvas' contents with the image contained in imageBlob */
function updateCanvas(imageBlob) {
    curImgBlob = imageBlob;
    if(imageBlob){
        var ctx = canvas.getContext('2d');

        var img = new Image();

        img.onload = function(){
            canvas.width = this.width;
            canvas.height = this.height;

            ctx.drawImage(img, 0, 0);
        };
        var URLObj = window.URL || window.webkitURL;
        img.src = URLObj.createObjectURL(imageBlob);
    }
}

const logInterp = (low, lowVal, mid, hi, hiVal) => {
    const f = (mid - low) / (hi - low);
    return hiVal**f * lowVal**(1 - f);
}

const linearInterp = (low, lowVal, mid, hi, hiVal) => {
    const f = (mid - low) / (hi - low)
    return (hiVal - lowVal) * f + lowVal;
}

/* Canvas click handler:
    If in boundary select state, add the specified boundary
    Otherwise, trace the line that was clicked on
*/
canvas.addEventListener('click', e => {
    // boundary select state
    if (selectingBoundary) {
        const ctx = canvas.getContext('2d');
        ctx.lineWidth = 2;
        ctx.strokeStyle = "rgb(255,0,255)";
        if (['top', 'bottom'].includes(selectingBoundary)) {
            boundaries[selectingBoundary] = e.offsetY;
            const y = Math.floor(e.offsetY * canvas.height / canvas.getBoundingClientRect().height)
            ctx.beginPath();
            ctx.moveTo(0, y);
            ctx.lineTo(canvas.width, y);
            ctx.closePath();
            ctx.stroke();
        } else {
            boundaries[selectingBoundary] = e.offsetX;
            const x = Math.floor(e.offsetX * canvas.width / canvas.getBoundingClientRect().width)
            ctx.beginPath();
            ctx.moveTo(x,0);
            ctx.lineTo(x, canvas.height);
            ctx.stroke();
        }
        document.getElementById(`${selectingBoundary}-boundary-select`).classList.replace('selecting', 'selected');
        selectingBoundary = null;

        if (boundaries.top && boundaries.bottom && boundaries.left && boundaries.right)
            document.getElementById('msg-container').innerText = "Click on the line you want to trace";
    }
    
    // all boundaries specified, trace the line and populate the download file
    else if (boundaries.top && boundaries.bottom && boundaries.left && boundaries.right) {
        const minX = +document.getElementById('left-boundary-value').value
        const maxX = +document.getElementById('right-boundary-value').value
        const minY = +document.getElementById('bottom-boundary-value').value
        const maxY = +document.getElementById('top-boundary-value').value
        const xInterp = document.getElementById('horizontal-log').checked ? logInterp : linearInterp;
        const yInterp = document.getElementById('vertical-log').checked ? logInterp : linearInterp;
        const zmaMode = document.getElementById('zma').checked;

        // get image-space coordinates of the pixel that was clicked
        const imgPixel = [e.offsetX * canvas.width / canvas.getBoundingClientRect().width, e.offsetY * canvas.height / canvas.getBoundingClientRect().height].map(Math.floor)
        const imgData = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);
        const dataOffset = 4 * imgData.width * imgPixel[1] + 4 * imgPixel[0];

        // get reference color
        const color = imgData.data.slice(dataOffset, dataOffset + 3);
        //console.log(`%c${color}`, `color:rgb(${color[0]},${color[1]},${color[2]})`);

        const diffSquare = (a,b) => a.reduce((acc, cur, idx) => acc + (cur - b[idx])**2, 0);

        const ctx = canvas.getContext('2d');
        ctx.lineWidth = 1;
        ctx.strokeStyle = "rgb(0,255,255)";

        const points = [imgPixel];

        // loop from the x value that was clicked to the right border of the image, appending points array with the nearest pixel that is within the color difference threshold
        let prevY = imgPixel[1];
        ctx.beginPath();
        ctx.moveTo(imgPixel[0], imgPixel[1]);
        for (let curX = imgPixel[0] + 1; curX < boundaries.right * canvas.width / canvas.getBoundingClientRect().width; ++curX) {
            for (let yOff = 0; prevY + yOff < boundaries.bottom * canvas.height / canvas.getBoundingClientRect().height || prevY - yOff > boundaries.top * canvas.height / canvas.getBoundingClientRect().height; ++yOff) {
                if (prevY + yOff < boundaries.bottom * canvas.height / canvas.getBoundingClientRect().height) {
                    let tempOffset = 4 * imgData.width * (prevY + yOff) + 4 * curX;
                    if (diffSquare(imgData.data.slice(tempOffset, tempOffset + 3), color) < COLOR_SQUARE_ERR_THRESHOLD) {
                        points.push([curX, prevY + yOff]);
                        ctx.lineTo(curX, prevY + yOff);
                        prevY += yOff;
                        break;
                    }
                }
                if (prevY - yOff > boundaries.top * canvas.height / canvas.getBoundingClientRect().height) {
                    let tempOffset = 4 * imgData.width * (prevY - yOff) + 4 * curX;
                    if (diffSquare(imgData.data.slice(tempOffset, tempOffset + 3), color) < COLOR_SQUARE_ERR_THRESHOLD) {
                        points.push([curX, prevY - yOff]);
                        ctx.lineTo(curX, prevY - yOff);
                        prevY -= yOff;
                        break;
                    }
                }
            }
        }
        ctx.stroke();

        // loop from the x value that was clicked to the left border of the image, prepending points array with the nearest pixel that is within the color difference threshold
        ctx.beginPath();
        ctx.moveTo(imgPixel[0], imgPixel[1]);
        prevY = imgPixel[1];
        for (let curX = imgPixel[0] - 1; curX > boundaries.left * canvas.width / canvas.getBoundingClientRect().width; --curX) {
            for (let yOff = 0; prevY + yOff < boundaries.bottom * canvas.height / canvas.getBoundingClientRect().height || prevY - yOff > boundaries.top * canvas.height / canvas.getBoundingClientRect().height; ++yOff) {
                if (prevY + yOff < boundaries.bottom * canvas.height / canvas.getBoundingClientRect().height) {
                    let tempOffset = 4 * imgData.width * (prevY + yOff) + 4 * curX;
                    if (diffSquare(imgData.data.slice(tempOffset, tempOffset + 3), color) < COLOR_SQUARE_ERR_THRESHOLD) {
                        points.unshift([curX, prevY + yOff]);
                        ctx.lineTo(curX, prevY + yOff);
                        prevY += yOff;
                        break;
                    }
                }
                if (prevY - yOff > boundaries.top * canvas.height / canvas.getBoundingClientRect().height) {
                    let tempOffset = 4 * imgData.width * (prevY - yOff) + 4 * curX;
                    if (diffSquare(imgData.data.slice(tempOffset, tempOffset + 3), color) < COLOR_SQUARE_ERR_THRESHOLD) {
                        points.push([curX, prevY - yOff]);
                        ctx.lineTo(curX, prevY - yOff);
                        prevY -= yOff;
                        break;
                    }
                }
            }
        }
        ctx.stroke();
        document.getElementById('download-button').removeAttribute('disabled');
        document.getElementById('download-link').setAttribute('href', `data:,${
            points.map(([x,y]) => [
                xInterp(boundaries.left * canvas.width / canvas.getBoundingClientRect().width, minX, x, boundaries.right * canvas.width / canvas.getBoundingClientRect().width, maxX),  // frequency
                yInterp(boundaries.bottom * canvas.height / canvas.getBoundingClientRect().height, minY, y, boundaries.top * canvas.height / canvas.getBoundingClientRect().height, maxY),  // amplitude
                0   // phase: not typically given in manufacturer response graphs, typical behavior of response graph tracing programs is to always set phase to 0 degrees
            ].join('%09')).join('%0A')}`);
        document.getElementById('download-link').setAttribute('download', zmaMode ? 'trace.zma' : 'trace.frd');
        document.getElementById('msg-container').innerText = 'Trace finished, click Download to download the file or Reset to try again';
    }
});

/* forward download button click to hidden <a> tag */
function download() {
    document.getElementById('download-link').click();
}